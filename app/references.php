<?php

namespace App;

use Cviebrock\EloquentTaggable\Taggable;
use Illuminate\Database\Eloquent\Model;

class references extends Model
{
    use Taggable;
    public $table = "references";

    protected $fillable = ['group_id', 'group', 'url', 'title'];

    function groups(){
        return $this->belongsTo('App\Group');
    }
}
