<?php
use Illuminate\Http\Request;
use  Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 *  @uses Illuminate\Support\Facades\Route;
 */
Route::apiResources([
    'services' => 'API\ServicesController',
]);

Route::apiResources([
    'website_sections' => 'API\Website_sectionController',
]);

Route::apiResources([
    'references' => 'API\ReferencesController',
]);

Route::apiResources([
    'group' => 'API\GroupController',
]);
