<?php

namespace App\Http\Controllers;

use App\website_section;
use Illuminate\Http\Request;

class Website_sectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $website_sections = Website_section::all();
            return view('website_section.index',compact('website_sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('website_section.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $website_section = new Website_section();
        $website_section->menu_title = $request->input['menu_title'];
        $website_section->title = $request->input['title'];
        $website_section->content = $request->input['content'];
        $website_section->save();
        return redirect('../public/website_sections');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Website_section $website_section)
    {
        return view('website_section.show',  compact('website_section'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Website_section $website_section)
    {
        return view('website_section.edit',  compact('website_section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Website_section $website_section)
    {
        $website_section->menu_title = $request['menu_title'];
        $website_section->title = $request['title'];
        $website_section->content = $request['content'];
        $website_section->save();
        return redirect('../public/website_sections');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Website_section $website_section)
    {
        $website_section->delete();
        return redirect('../public/website_sections');
    }
}
