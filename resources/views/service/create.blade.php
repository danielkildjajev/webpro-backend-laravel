@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Add Service</h1>
        <form method="POST" action="{{ route('services.store') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <h3>Title</h3>
                <input type="text" name="title" value="" placeholder="title">
            </div>
            <div class="form-group">
                <h3>Content</h3>
            <input type="text" name="content" value="" placeholder="content">
            </div>
                <div class="form-group">
                    <h3>Image</h3>
            <input type="text" name="image" value="" placeholder="image">
                </div>
            <input type="submit" value="Submit" class="btn btn-primary">
        </form>
    </div>
@endsection
