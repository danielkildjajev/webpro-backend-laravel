@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Add Section</h1>
        <form method="POST" action="{{ route('website_sections.store') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <h3>Menu_title</h3>
                <input type="text" name="menu_title" value="" placeholder="menu_title">
            </div>
            <div class="form-group">
                <h3>Title</h3>
                <input type="text" name="title" value="" placeholder="title">
            </div>
            <div class="form-group">
                <h3>Content</h3>
                <input type="text" name="content" value="" placeholder="content">
                @section('js')
                    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
                    <script>
                        CKEDITOR.replace( 'content' );
                    </script>
                @append
            </div>
            <input type="submit" value="Submit" class="btn btn-primary">
        </form>
    </div>
@endsection

