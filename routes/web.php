<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function () {
    Route::resource('services', 'ServiceController');
    Route::resource('website_sections', 'Website_sectionController');
    Route::resource('references', 'ReferencesController');
    Route::resource('groups', 'GroupController');
});
//Route::post('/konkurss/{konkurss}/detail', 'PersonController@store')->name('storeprojects');


