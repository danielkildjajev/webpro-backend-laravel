@extends('layouts.app')

@section('content')
    <div class="container">
        <p><span>Id: </span>{{ $website_section->id }}</p>
        <p><span>Menu_Title: </span>{{ $website_section->menu_title }}</p>
        <p><span>Title: </span>{{ $website_section->title }}</p>
        <p><span>Content: </span>{{ $website_section->content }}</p>
        <a href="{{ URL('website_sections/')}}" class="btn btn-danger">Back</a>
    </div>
@endsection
