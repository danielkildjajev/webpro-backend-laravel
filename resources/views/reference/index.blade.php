@extends('layouts.app')

@section('content')

    <div class="container">
        <table>
            <th>Reference Name /</th>
            <th>Group</th>
            <th><a href="{{ URL('api/references')}}" class="btn btn-danger">Api</a></th>
        </table>
        @foreach($references as $reference)
            <table>
                <tr>
                    <td>{{  $reference->title .' /'}}</td>
                    <td>{{ $reference->group }}</td>
                    <td><a href="{{ URL('references/'.$reference->id )}}" class="btn btn-success">Info</a></td>
                    <td><a href="{{ URL('references/'.$reference->id.'/edit/' )}}" class="btn btn-warning">Edit</a></td>
                    <td><form action="{{ route('references.destroy', $reference->id)}}" method="post">
                            @csrf
                            @method('DELETE')

                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form></td>
                </tr>
            </table>

        @endforeach
    </div>


@endsection
