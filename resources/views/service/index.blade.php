@extends('layouts.app')

@section('content')

<div class="container">
    <table>
        <th>Service Name</th>
        <th><a href="{{ URL('api/services')}}" class="btn btn-danger">Api</a></th>
    </table>
    @foreach($services as $service)
        <table>
            <tr>
                <td>{{  $service->title }}</td>
                <td><a href="{{ URL('services/'.$service->id )}}" class="btn btn-success">Info</a></td>
                <td><a href="{{ URL('services/'.$service->id.'/edit/' )}}" class="btn btn-warning">Edit</a></td>
                <td><form action="{{ route('services.destroy', $service->id)}}" method="post">
                        @csrf
                        @method('DELETE')

                        <button class="btn btn-danger" type="submit">Delete</button>

                    </form></td>
            </tr>
        </table>

    @endforeach
</div>


@endsection
