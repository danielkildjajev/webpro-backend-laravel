@extends('layouts.app')

@section('content')

    <div class="container">
        <table>
            <th>Group Name</th>
            <th><a href="{{ URL('api/group')}}" class="btn btn-danger">Api</a></th>
        </table>
        @foreach($groups as $group)
            <table>
                <tr>
                    <td>{{  $group->group_title }}</td>
                    <td><a href="{{ URL('groups/'.$group->id )}}" class="btn btn-success">Info</a></td>
                    <td><a href="{{ URL('groups/'.$group->id.'/edit/' )}}" class="btn btn-warning">Edit</a></td>
                    <td><form action="{{ route('groups.destroy', $group->id)}}" method="post">
                            @csrf
                            @method('DELETE')

                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form></td>
                </tr>
            </table>

        @endforeach
    </div>


@endsection
