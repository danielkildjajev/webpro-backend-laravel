@extends('layouts.app')

@section('content')
    <div class="container">
        <p><span>Id: </span>{{ $service->id }}</p>
        <p><span>Image: </span><img src="{{ asset($service->image) }}" style="width:250px;height:250px;"></p>
        <p><span>Name: </span>{{ $service->title }}</p>
        <p><span>Content: </span>{{ $service->content }}</p>
        <a href="{{ URL('services/')}}" class="btn btn-danger">Back</a>
    </div>
@endsection
